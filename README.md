# compass-container

## Overview

This repository contains the Singularity definition for building a container containing the following:
* Tensorflow
* Torch

See the compass-singularity.def file for a complete listing of included libraries.

Services include Jupyter.

The definition file builds directly from the official Ubuntu 20.04 (Focal) Docker image.

## Build

`sudo singularity build tensorflow-torch-singularity.sif tensorflow-torch-singularity.def`

## Pull Image File

`singularity pull oras://gitlab-registry.oit.duke.edu/chsi-informatics/containers/tensorflow-torch-container/tensorflow-torch-container:latest`

## Run

Running the container launches a Jupyter session on port `8888`. Any directory bindings must be explicitly set when issuing the `singularity run` command.

To run with CUDA enabled TensorFlow, use the `--nv` flag for the `run` sub-command:

`singularity run --nv tensorflow-torch-singularity.sif`

### Jupyter
```
singularity exec tensorflow-torch-singularity.sif jupyter notebook
```
